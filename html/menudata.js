/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var menudata={children:[
{text:"Página principal",url:"index.html"},
{text:"Páginas relacionadas",url:"pages.html"},
{text:"Namespaces",url:"namespaces.html",children:[
{text:"Lista de 'namespaces'",url:"namespaces.html"},
{text:"Miembros del Namespace ",url:"namespacemembers.html",children:[
{text:"Todo",url:"namespacemembers.html",children:[
{text:"f",url:"namespacemembers.html#index_f"},
{text:"h",url:"namespacemembers.html#index_h"},
{text:"i",url:"namespacemembers.html#index_i"},
{text:"m",url:"namespacemembers.html#index_m"},
{text:"o",url:"namespacemembers.html#index_o"},
{text:"p",url:"namespacemembers.html#index_p"},
{text:"r",url:"namespacemembers.html#index_r"},
{text:"s",url:"namespacemembers.html#index_s"},
{text:"t",url:"namespacemembers.html#index_t"},
{text:"w",url:"namespacemembers.html#index_w"}]},
{text:"Funciones",url:"namespacemembers_func.html"},
{text:"Variables",url:"namespacemembers_vars.html",children:[
{text:"f",url:"namespacemembers_vars.html#index_f"},
{text:"h",url:"namespacemembers_vars.html#index_h"},
{text:"i",url:"namespacemembers_vars.html#index_i"},
{text:"m",url:"namespacemembers_vars.html#index_m"},
{text:"p",url:"namespacemembers_vars.html#index_p"},
{text:"r",url:"namespacemembers_vars.html#index_r"},
{text:"s",url:"namespacemembers_vars.html#index_s"},
{text:"t",url:"namespacemembers_vars.html#index_t"},
{text:"w",url:"namespacemembers_vars.html#index_w"}]}]}]},
{text:"Clases",url:"annotated.html",children:[
{text:"Lista de clases",url:"annotated.html"},
{text:"Índice de clases",url:"classes.html"},
{text:"Miembros de las clases",url:"functions.html",children:[
{text:"Todo",url:"functions.html"},
{text:"Variables",url:"functions_vars.html"}]}]},
{text:"Archivos",url:"files.html",children:[
{text:"Lista de archivos",url:"files.html"},
{text:"Miembros de los ficheros",url:"globals.html",children:[
{text:"Todo",url:"globals.html"},
{text:"Funciones",url:"globals_func.html"},
{text:"Variables",url:"globals_vars.html"},
{text:"defines",url:"globals_defs.html"}]}]}]}
