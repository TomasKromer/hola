"""
*  @file Plotter.py
 * @brief This program is in charge of subscribing to the Broker, receiving the data, 
 * saving it in a csv and graphing the received data.
 *
 * @version 0.1
 * @date 2021-09-02
 *
 * @copyright Copyright (c) 2021
 *
"""
import paho.mqtt.client as mqtt
from const import *
from numpy import *
from math import *
import matplotlib.pyplot as plt
import sys
import csv
import pandas as pd
import pathlib
import os

semOne = False
SemTwo = False
semThree = False
semFour = False
semFive = False
semSix= False
semSeven= False
samples = [0,0,0,0]
receiverDecode = []
receiverDecodeNahuel = []
receiverDecodeDiego = []
receiverDecodeMayra = []
receiverDecodeTomas = []

if sys.version >= '3':
    file = open("data.csv",newline = "")
else:
	file = open("data.csv")

path = str(pathlib.Path().absolute()) + "/data.csv"
writer = csv.writer(file)

def on_connect(client, userdata, flags, rc):
	print('connected (%s)' % client._client_id)
	client.subscribe(Topic)

"""
  @file on_message
  @brief This function works when it receives the data, populates the lists and packages, 
  builds the csv and graphics
"""
def on_message(client, userdata, message):
	global semSeven
	global semSix
	global semFive
	global semFour
	global semThree
	global semOne
	receiver = str(message.payload.decode("utf-8") )
	receiverJson = receiver.split(';')
	if samples[IndexNahuel] < maxSample or samples[IndexDiego] < maxSample or samples[IndexMayra] < maxSample  or samples[IndexTomas] < maxSample :
		if semOne == False:
			for i in range(FieldZero, FieldThree, 1):
				receiverDecode.append(receiverJson[i].split('=')[IndexTittle])
			writer.writerow(receiverDecode)
			receiverDecode.clear()
			semOne = True
		for i in range(FieldZero, FieldThree, 1):
			if int(receiverJson[IndexId].split('=')[IndexValue]) == IndexNahuel and semFour == False:
				receiverDecodeNahuel.append(receiverJson[i].split('=')[IndexValue])
				if samples[IndexNahuel] < maxSample:
					semFor = False
				else :
					semFour = True
			elif int(receiverJson[IndexId].split('=')[IndexValue]) == IndexDiego and semFive == False:
				receiverDecodeDiego.append(receiverJson[i].split('=')[IndexValue])
				if samples[IndexDiego] < maxSample:
					semFive = False
				else :
					semFive = True
			elif int(receiverJson[IndexId].split('=')[IndexValue]) == IndexMayra and semSix == False:
				receiverDecodeMayra.append(receiverJson[i].split('=')[IndexValue])
				if samples[IndexMayra] < maxSample:
					semSix = False
				else :
					semSix = True
			elif int(receiverJson[IndexId].split('=')[IndexValue]) == IndexTomas and semSeven == False:
				receiverDecodeTomas.append(receiverJson[i].split('=')[IndexValue])
				if samples[IndexTomas] < maxSample:
					semSeven = False
				else :
					semSeven = True
		if int(receiverJson[IndexId].split('=')[IndexValue]) == IndexNahuel and semFour == False:
			writer.writerow(receiverDecodeNahuel)
			print(receiverDecodeNahuel)
			receiverDecodeNahuel.clear()
		elif int(receiverJson[IndexId].split('=')[IndexValue]) == IndexDiego and semFive == False:
			writer.writerow(receiverDecodeDiego)
			receiverDecodeDiego.clear()
		elif int(receiverJson[IndexId].split('=')[IndexValue]) == IndexMayra and semSix == False:
			writer.writerow(receiverDecodeMayra)
			receiverDecodeMayra.clear()
		elif int(receiverJson[IndexId].split('=')[IndexValue]) == IndexTomas and semSeven == False:
			writer.writerow(receiverDecodeTomas)
			receiverDecodeTomas.clear()
		samples[IndexNahuel] += 1
		samples[IndexDiego] += 1
		samples[IndexMayra] += 1
		samples[IndexTomas] += 1
	else:
		if semThree == False :
			if file is not None :
				file.seek(0)
				datos = pd.read_csv(file)
				fig = plt.figure()
				plt.plot(datos[' timestamp '], datos[' temp '])		
				plt.show()		
				semThree = True

"""
  @file main
 @brief main function
"""
def main():
	client = mqtt.Client()
	client.on_connect = on_connect
	client.on_message = on_message
	client.connect(Host, port)
	client.loop_forever()

if __name__ == '__main__':
	main()

sys.exit(0)

